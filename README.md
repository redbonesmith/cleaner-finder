## install

install postgresql with tools

```sh
$ sudo apt-get install postgresql postgresql-contrib
```

# run
```sh
$ npm install
$ ./bin/server
```

# run from gulp
```sh
$ gulp get-matches --country=de --geo=54.312,14.7878
```

#request

```sh
# http get "http://localhost:2000/match/country/de/geo/54.1212,54.42424?gender=f&preferences=one,two"
```
