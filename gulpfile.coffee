gulp = require 'gulp'
cleaners = require './src/services/cleaners_matching'
request = require 'request-promise'
_ = require 'lodash'

gulp.task 'get-matches', ->
  args = process.argv.slice(3)
  req = args.map (arg) ->
    doc = {}
    doc[arg.split('=')[0].replace(/\-/g,'')] = arg.split('=')[1]
    return doc

  country = _.find req, (r) -> r.country
  geo = _.find req, (r) -> r.geo
  gender = _.find req, (r) -> r.gender
  preferences = _.find req, (r) -> r.preferences

  url = "http://localhost:2000/match/country/#{country.country}/geo/#{geo.geo}"

  url += gender.gender if gender
  url += preferences.preferences if preferences

  request url
  .then (result) -> console.log result
  .catch (err) -> console.error {err}
