geolib = require 'geolib'
config = require '../config'

module.exports.isCleanerInArea = (cleanerCoordinates, centerCoordinates, countryData) ->
  distance = if countryData.unit is 'km' then countryData.distance * 1000 else countryData.distance

  return geolib.isPointInCircle(cleanerCoordinates, centerCoordinates, distance)
