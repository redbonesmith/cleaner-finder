db = require '../database'
_ = require 'lodash'

SQL = '../sql'


class Cleaners
  @getAllCleanersByCountry: (countryCode) ->
    db.manyOrNone SQL.cleaners_by_country(countryCode)
    .then (cleaners) ->
      console.log "found #{cleaners.length} cleaner[s] by '#{countryCode}' country code"
      _(cleaners).unique().value()
    .catch (err) -> console.error {err, module: 'models.cleaners'}

module.exports = Cleaners
