Promise = require 'bluebird'
_ = require 'lodash'

Cleaners = require '../models/cleaners'
{isCleanerInArea} = require '../models/geolocation'

config = require '../config'
logger = require '../logger'

exports.getMatches = (req, res) ->
  {center, countryCode} = req.params
  {gender, preferences} = req.query

  return res.status(400).json(error: 'no countryCode') unless countryCode
  return res.status(400).json(error: 'no center') unless center

  parsedGeoParams = center.split(',')
  return res.status(400).json(error: 'wrong geo params') unless parsedGeoParams.length is 2

  center =
    latitude: parsedGeoParams[0]
    longitude: parsedGeoParams[1]

  preferences = preferences.split(',') if preferences

  Cleaners = new Cleaners()
  Cleaners.getAllCleanersByCountry(countryCode)
  .then (cleaners) ->
  if _.isEmpty cleaners
    logger.err "cleaners not found by #{countryCode}"
    return res.status(404).json(error: 'cleaners not found')

  matches = _.filter cleaners, (cleaner) ->
    {latitude, longitude, country_code} = cleaner
    countryData = config.COUNTRY_CONSTANTS[country_code]

    return isCleanerInArea({latitude, longitude}, center, countryData)

  logger.info cleaners.length, countryCode, center

  matches = _.filter matches, (cleaner) ->
    return matches unless gender

    cleaner.gender is gender unless _.isEmpty(matches)

  logger.info cleaners.length, countryCode, center, gender

  unless _.isEmpty preferences
    matches = _.filter matches, (cleaner) ->
      _.intersection(preferences, matches).length >= cleaner.length unless _.isEmpty(matches)

    logger.info cleaners.length, countryCode, center, gender, preferences

  return matches
