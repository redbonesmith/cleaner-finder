module.exports =
  cleaners_by_country: (countryCode) ->
    return "select i.name, i.gender, i.country_code, i.latitude, i.longitude, string_agg(p.name, ', ') as preferences
    from cleaners i
    join cleaners_preferences cp on i.id = cp.cleaner_id
    join preferences p on p.id = cp.preference_id
    where country_code = '#{countryCode}'
    group by i.name, i.gender, i.country_code, i.latitude, i.longitude;"
