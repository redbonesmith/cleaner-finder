http = require 'http'
app = require './app'

config = require './config'

server = http.createServer app

onListen = (err) ->
  if err
    console.error err, 'error starting'
    process.exit 1

  address = server.address()
  console.log "listening on http://#{address.address}:#{address.port}"

server.once 'error', (err) ->
  return onListen err if err.code == 'EADDRINUSE'
  throw err

if config.HOST == '0.0.0.0' or config.HOST == '::'
  server.listen config.PORT, onListen
else
  server.listen config.PORT, config.HOST, onListen
