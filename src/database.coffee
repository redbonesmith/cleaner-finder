type = 'database'
#TODO change to driver : http://docs.sequelizejs.com/en/latest/docs/getting-started/
pg = require 'pg'
Promise = require('bluebird')
pgp = require('pg-promise')(promiseLib: Promise)

config = require './config'

db = pgp config.POSTGRES_URI

pg.on 'error', (err) ->
  console.error {err, type} if err?

db.connect()
.catch (err) ->
  console.error {error: err.code, type}

module.exports = db
