config = module.exports

config.PROJECT_NAME = process.env.PROJECT_NAME or 'bookatiger'
config.PORT = parseInt(process.env.PORT or '2000', 10)

config.LOG_LEVEL = process.env.LOG_LEVEL or 'debug'

config.POSTGRES_URI = process.env.POSTGRES_URI or 'postgres://secret:secret@localhost:5432/cleaners'

config.COUNTRY_CONSTANTS =
  de:
    distance: 10
    unit: 'km'
  au:
    distance: 20
    unit: 'km'
  ch:
    distance: 17.5
    unit: 'km'
  nl:
    distance: 9
    unit: 'km'
