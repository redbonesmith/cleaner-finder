express = require 'express'
cleanersService = require './services/cleaners_matching'

router = express.Router()

router.get '/match/country/:countryCode/geo/:center', cleanersService.getMatches

module.exports = router
